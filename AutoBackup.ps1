﻿$Rechner = "localhost"
[string]$Username = Get-WmiObject Win32_ComputerSystem -ComputerName $Rechner | Select-Object UserName
[array]$username = $username.split("\") #Username von der Domäne trennen
[string]$username = $username[1]
$username = $username.replace("}","") #} hinter dem Benutzer mit " " ersetzen, damit man nur den Namen hat

if($username -eq 'maya.stamm')
{
    Copy-Item "C:\Users\maya.stamm\Desktop\*" "\\192.168.4.3\Abwicklung\BackupMaja"

    Copy-Item "C:\Users\maya.stamm\Desktop\*" "\\192.168.4.3\Geschaeftsleitung\BackupMaja"
}
if($username -eq 'lars.wyler')
{
    Copy-Item "C:\Users\lars.wyler\Desktop\*" "\\192.168.4.3\Geschaeftsleitung\BackupLars"
}
if($username -eq 'severin.meier')
{
    Copy-Item "C:\Users\severin.meier\Desktop\*" "\\192.168.4.3\Marketing_Sales\BackupSeverin"
}
if($username -eq 'gian.sommer')
{
    Copy-Item "C:\Users\gian.sommer\Desktop\*" "\\192.168.4.3\Marketing_Sales\BackupGian"
}
if($username -eq 'jake.dude')
{
    Copy-Item "C:\Users\jake.dude\Desktop\*" "\\192.168.4.3\Design_Publishing\BackupJake"
}
if($username -eq 'admir.duraso')
{
    Copy-Item "C:\Users\admir.duraso\Desktop\*" "\\192.168.4.3\Design_Publishing\BackupAdmir"
}
if($username -eq 'lorenz.fischer')
{
    Copy-Item "C:\Users\lorenz.fischer\Desktop\*" "\\192.168.4.3\Design_Publishing\BackupLorenz"
}
if($username -eq 'theo.kranz')
{
    Copy-Item "C:\Users\theo.kranz\Desktop\*" "\\192.168.4.3\IT Administration\BackupLorenz"
}
else
{
    Write-Host "User kann leider nicht gefunden werden"
}