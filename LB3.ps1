﻿Start-Transcript -Path 'C:\Users\Phili\Desktop\Schule\TBZ\Modul 143\Logs\log.log'
function FullBackup 
{
    $Source = $Global:HomePfad
    $Destination = $Global:NASPfad
    $date = Get-Date -Format d #Datum ausgeben im Format: dd.MM.yyyy
    $Folder = $Date + " "+ $Username
    $validation = Test-Path $Destination
    robocopy $Source $Destination\$Folder *.* /mir /sec
}
$SucheHome =
{
    $keyCode = ([int]$_.KeyChar)
    if ($keyCode -eq 13)
    {
        Write-Host $TextBoxQuelle.Text #Wenn man "Enter" drückt wird dieser Text in die Variabel $TextBoxQuelle gespeichert
        $Global:HomePfad = $TextBoxQuelle.Text #Global, weil man diese Variabel sonst nicht in anderen Funktionen verwenden kann
    }
}
$SucheNAS =
{
    $keyCode = ([int]$_.KeyChar)
    if ($keyCode -eq 13)
    {
        Write-Host $TextBoxNAS.Text #Wenn man "Enter" drückt wird dieser Text in die Variabel $TextBoxNAS gespeichert
        $Global:NASPfad = $TextBoxNAS.Text #Global, weil man diese Variabel sonst nicht in anderen Funktionen verwenden kann
    }
}
$Restore =
{
    Copy-Item "C:\Users\Phili\Desktop\Schule\TBZ\Modul 143\Backup\26.01.2019 Phili\*" "C:\Users\Phili\Desktop\Schule\TBZ\Modul 143\LB 3\Test"
}
$StoppBackup =
{
    Write-Host "Backup wurde gestoppt"
}

# Fenster 1 (Home Screen)
$BackupFenster = New-Object Windows.Forms.Form #Fenster erstellen
$BackupFenster.Size = New-Object System.Drawing.Size(660,250) #
$BackupFenster.Font = New-Object System.Drawing.Font("timesnewroman",12,[System.Drawing.FontStyle]::Regular)
$BackupFenster.StartPosition = "CenterScreen" #Fenster öffnet sich in der mitte des PC's
$BackupFenster.Text = "LB 3" #Titel des Fensters

$Rechner = "localhost"
[string]$Username = Get-WmiObject Win32_ComputerSystem -ComputerName $Rechner | Select-Object UserName
[array]$username = $username.split("\") #Username von der Domäne trennen
[string]$username = $username[1]
$username = $username.replace("}","") #} hinter dem Benutzer mit " " ersetzen, damit man nur den Namen hat

$FullBackupButton = New-Object System.Windows.Forms.Button #Button für ein Full Backup erstellen
$FullBackupButton.Text = "Full Backup"
$FullBackupButton.Add_Click({FullBackup})
$FullBackupButton.Left = "160"
$FullBackupButton.Top = "120"
$FullBackupButton.Width = "150"
$FullBackupButton.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

$LabelTitel = New-Object System.Windows.Forms.Label
$LabelTitel.Text = "Backup und Restore Ihrer Daten:"
$LabelTitel.Left = "105"
$LabelTitel.Top = "0"
$LabelTitel.Width = "450"
$LabelTitel.Height = "30"
$LabelTitel.Font = New-Object System.Drawing.Font("Algerian",12,[System.Drawing.FontStyle]::Regular)

$LabelQuellen = New-Object System.Windows.Forms.Label
$LabelQuellen.Text = "Quelle (HOME):"
$LabelQuellen.Left = "10"
$LabelQuellen.Top = "51"
$LabelQuellen.Width = "125"
$LabelQuellen.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

$LabelBackupZiel = New-Object System.Windows.Forms.Label
$LabelBackupZiel.Text = "Ziel Ort (NAS):"
$LabelBackupZiel.Left = "10"
$LabelBackupZiel.Top = "81"
$LabelBackupZiel.Width = "125"
$LabelBackupZiel.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

$LabelBackupDienst = New-Object System.Windows.Forms.Label
$LabelBackupDienst.Text = "Backupdienst:"
$LabelBackupDienst.Left = "10"
$LabelBackupDienst.Width = "125"
$LabelBackupDienst.Top = "120"
$LabelBackupDienst.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

$TextBoxQuelle = New-Object System.Windows.Forms.TextBox
$TextBoxQuelle.Text = "Pfad auf dem lokalen PC"
$TextBoxQuelle.Add_Click({$TextBoxQuelle.Text = ""})
$TextBoxQuelle.Add_KeyPress($SucheHome)
$TextBoxQuelle.Left = "160"
$TextBoxQuelle.Top = "50"
$TextBoxQuelle.Width = "470"

$TextBoxNAS = New-Object System.Windows.Forms.TextBox
$TextBoxNAS.Text = "Pfad auf dem NAS z.B. \\192.168.4.3\"
$TextBoxNAS.Add_Click({$TextBoxNAS.Text = ""})
$TextBoxNAS.Add_Keypress($SucheNAS)
$TextBoxNAS.Left = "160"
$TextBoxNAS.Top = "80"
$TextBoxNAS.Width = "470"

$ButtonStopp = New-Object System.Windows.Forms.Button
$ButtonStopp.Text = "Stopp"
$ButtonStopp.Add_Click($StoppBackup)
$ButtonStopp.Left = "480"
$ButtonStopp.Top = "120"
$ButtonStopp.Width = "150"
$ButtonStopp.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

$ButtonRestore = New-Object System.Windows.Forms.Button
$ButtonRestore.Text = "Restore"
$ButtonRestore.Add_Click($Restore)
$ButtonRestore.Left = "320"
$ButtonRestore.Top = "120"
$ButtonRestore.Width = "150"
$ButtonRestore.Font = New-Object System.Drawing.Font("Arial",8,[System.Drawing.FontStyle]::Regular)

#Funktionen hinzufügen
$BackupFenster.Controls.Add($FullBackupButton)
$BackupFenster.Controls.Add($LabelQuellen)
$BackupFenster.Controls.Add($LabelBackupZiel)
$BackupFenster.Controls.Add($ButtonStopp)
$BackupFenster.Controls.Add($ButtonRestore)
$BackupFenster.Controls.Add($TextBoxQuelle)
$BackupFenster.Controls.Add($TextBoxNAS)
$BackupFenster.Controls.Add($LabelBackupDienst)
$BackupFenster.Controls.Add($LabelTitel)

#Formular starten und anzeigen
$BackupFenster.ShowDialog() | Out-Null

do{

$Confirm = New-Object Windows.Forms.Form #Fenster erstellen
$Confirm.Size = New-Object System.Drawing.Size(500,201) # Fenstergrösse
$Confirm.StartPosition = "CenterScreen" #Fenster öffnet sich in der mitte des PC's
$Confirm.Text = "Backup Tool beenden?" #Titel des Fensters

$ConfirmLabel = New-Object System.Windows.Forms.Label #Label erstellen
$ConfirmLabel.Text = "Wollen Sie das Programm wirklich beenden?" #Titel des Fensters
$ConfirmLabel.ForeColor = 'Black' #Hintergrundfarbe Weiss
$ConfirmLabel.Width = 350
$ConfirmLabel.Height = 100
$ConfirmLabel.Left = 75
$ConfirmLabel.TextAlign = "MiddleCenter"
$FontColor = New-Object System.Drawing.Font("Arial", 14) # Grösse und Schriftart des Labels
$ConfirmLabel.Font = $FontColor

$ButtonJA = New-Object System.Windows.Forms.Button
$ButtonJA.Text = "Ja"
$ButtonJA.Left = 0
$ButtonJA.Width = 250
$ButtonJA.Height = 56
$ButtonJA.Top = 90
$ButtonJA.Add_Click({
    Write-Host "Das Backup Tool wurde erfolgreich beendet."
    })
$ButtonJA.BackColor = '#424242'
$ButtonJA.Font = $FontColor
$ButtonJA.ForeColor = 'White'

$ButtonNEIN = New-Object System.Windows.Forms.Button
$ButtonNEIN.Text = "Nein"
$ButtonNEIN.Left = 245
$ButtonNEIN.Width = 235
$ButtonNEIN.Height = 56
$ButtonNEIN.Top = 90
$ButtonNEIN.Add_Click({
    $Confirm.Dispose()
    $BackupFenster.ShowDialog()
    })
$ButtonNEIN.BackColor = '#424242'
$ButtonNEIN.Font = $FontColor
$ButtonNEIN.ForeColor = 'White'
$buttonJA.DialogResult = "OK"

$Confirm.Controls.Add($ButtonJA)
$Confirm.Controls.Add($ButtonNEIN)
$Confirm.Controls.Add($ConfirmLabel)
$CloseWindowResult = $Confirm.ShowDialog()


}while ($CloseWindowResult -ne "OK")
Stop-Transcript